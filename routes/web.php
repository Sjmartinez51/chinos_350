<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//ruta de prueba 
Route::get('hola',function(){echo "hola";});

//Ruta arreglo
Route::get('arreglo',function(){
    //defino un arreglo 
$estudiantes = [ "AN"
=> "Ana",
"M" => "Maria",
"VA" => "Valeria",
"CA" => "Carlos"];

//ciclo foreach recorrer arreglo
foreach($estudiantes as $indice => $e){
    echo "$e  tiene el indice: $indice <br />";
}

});

//ruta de paises

Route::get('paises',function(){
 $paises = [
   "Colombia" => [
   "Capital"=>"Bogotá",
   "Moneda"=>"Peso",
   "Población"=>50372424,
   "Ciudades" => ["Medellin" , "Cali" , "Barranquilla"]
],

    "Peru" => [
    "Capital"=>"Lima",
    "Moneda"=>"Sol",
    "Población"=> 33050325,
    "Ciudades" => ["Cuzco","Trujillo","Arequipa"]
],

    "Ecuador"  => [
    "Capital"=>"Quito",
    "Moneda"=>"Dolar",
    "Población" => 12517141,
    "Ciudades" => ["Guayaquil","Manta","Cuenca"]
],

   "Brazi" => [
   "Capital"=>"Bralizilia",
   "Moneda"=>"Real",
   "Población"=>212216052,
   "Ciudades"=>["Rio de Janeiro","Recife","Bahia"]
   ]
] ;

//Enviar datos de paises a una vista 
//Con la función view
return view ('paises')
  ->with("paises", $paises);

//Enviar datos a una vista 
//funcion view
return view('paises')
 ->with("paises" , $paises) ;
});