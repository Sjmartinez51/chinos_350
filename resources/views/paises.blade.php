<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
   <h1 class="text-danger">Lista de paises</h1> 
   <table class="table table-hover table-bordered">
   <thead>
   <tr>
   <th>Pais</th>
   <th>Capital</th>
   <th>Moneda</th>
   <th>Población</th>
   <th>Ciudades Principales</th>
   </tr>
   </thead>
   <tbody>
   @foreach($paises as $pais => $infopais)
   <tr>
   <td rowspan="3" > {{ $pais }} </td>
   <td rowspan="3" > {{  $infopais["Capital"] }}</td>
   <td rowspan="3" > {{ $infopais["Moneda"]  }}</td>
   <td rowspan="3" > {{ $infopais["Población"]  }}</td>
   <td> {{    $infopais["Ciudades"][0]  }}   </td>
   </tr>
   <tr> 
   <th> {{  $infopais["Ciudades"][1]}} </th></tr>
   <tr>
   <th>
   {{$infopais["Ciudades"][2]}}
   </th></tr>
   @endforeach


   </tbody>
   </table>
</body>
</html>